<?php defined('SYSPATH') OR die('No Direct Script Access');

class utilClass
{

    // ------------------------------------------------------------------------------------------------*
    // ---------------------------------- Methods convertions -----------------------------------------*
    /*    public static function converToText($xdate, $xtime = true)
        {
            if (empty($xdate))
                return self::converToText(date('d-m-Y H:i'), $xtime);

            $date = strtotime($xdate);
            $day = date('d', $date);
            $month = date('m', $date);
            $year = date('Y', $date);
            $time = date('H:i', $date);
            $resutl = "$day-$month-$year";
            if ($xtime)
                $resutl .= ' ' . $time;
            return $resutl;
        }*/

    public static function convertDate($xforUser, $xdate, $xtime = false)
    {
        if ($xforUser) {
            $date = date("d-m-Y", strtotime($xdate));
            if ($xtime)
                $date = date("d-m-Y H:i", strtotime($xdate));
        } else {
            $date = date("Y-m-d", strtotime($xdate));
            if ($xtime)
                $date = date("Y-m-d H:i", strtotime($xdate));
        }
        return $date;
    }

    public static function formatNumber($xnum)
    {
        return number_format($xnum, 2, ",", ".");
    }

    public static function checkDesimals($xvalue)
    {
        $punto = strpos($xvalue, '.');
        $coma = strpos($xvalue, ',');
        if ($punto < $coma)
            return str_replace('.', '', $xvalue);
        else {
            $xvalue = str_replace(',', '', $xvalue);
            return str_replace('.', ',', $xvalue);
        }
    }

    // ------------------------------------------------------------------------------------------------*
    // ---------------------------------- Methods of GETs ---------------------------------------------*

    public static function getTomorrow()
    {
        return date("Y-m-d", strtotime("+1 day"));
    }

    public static function startsWith($xtext, $xchar)
    {
        return strpos($xtext, $xchar) === 0;
    }

    public static function getCurrentURI()
    {
        if (isset($_SESSION[configClass::$c_currentURI]))
            return $_SESSION[configClass::$c_currentURI];
        else
            return "";
    }

    public static function getLastQuery()
    {
        return $_SESSION[configClass::$c_lastQuery];
    }

    public static function getShortText($xstring, $xlenght)
    {
        $newString = $xstring;
        if (strlen($xstring) > $xlenght) {
            $newString = substr($xstring, 0, $xlenght);
            $newString .= " ...";
        }
        return $newString;
    }

    public static function getUrlResource($xopt)
    {
        $uri = '';
        switch ($xopt) {
            case 1:
                $uri = URL::base(true) . configClass::$path_css;
                break;
            case 2:
                $uri = URL::base(true) . configClass::$path_js;
                break;
            case 3:
                $uri = URL::base(true) . configClass::$path_img;
                break;
        }
        return $uri;
    }

    public static function getLimitPage()
    {
        $pagina = 1;
        if (isset($_GET['page'])) {
            $pagina = (int)Arr::get($_GET, 'page');
            if ($pagina == 0)
                $pagina = 1;
            if ($pagina < 0)
                $pagina = $pagina * -1;
        }
        $num_por_pag = configClass::$perPage;

        $ini = ($pagina * $num_por_pag) - $num_por_pag;
        $fin = $num_por_pag;
        return array($ini, $fin, $pagina);
    }

    public static function showPaged($xview, $xpageCurrent, $xtotal)
    {
        if ($xtotal == 0)
            $xview->set("paginated", '');
        else {
            $vista_paged = View::factory('includes/paginate');
            $vista_paged->set('pageCurrent', $xpageCurrent);

            $pages = ceil($xtotal / configClass::$perPage);
            $vista_paged->set('pageTotal', $pages);
            if ($pages > 1) {
                $xview->set('paginated', $vista_paged);
            } else {
                $xview->set("paginated", '');
            }
        }
    }

    public static function getToday($xtime = false)
    {
        if ($xtime) {
            return date('Y-m-d H:i:s');
        } else {
            return date('Y-m-d');
        }
    }




    // ------------------------------------------------------------------------------------------------*
    // ---------------------------------- Methods of SETs ---------------------------------------------*

    public static function setMetas($xview, $xtitle, $xdes, $xkeyword)
    {
        $xview->titlePage = ($xtitle != "") ? $xtitle : configClass::$titleDefault;
        $xview->desPage = ($xdes != "") ? $xdes : configClass::$desDefault;
    }

    public static function setUppercaseFirst($xtext)
    {
        return ucwords($xtext);
    }

    public static function setCurrentURI()
    {
        $uri = $_SERVER['REQUEST_URI'];
        if (Kohana::$environment === Kohana::DEVELOPMENT)
            $uri = str_replace(Kohana::$base_url, "", $uri);
        else
            $uri = substr($uri, 1, strlen($uri) - 1);
        $uri = preg_replace("/\?.*/i", "", $uri);
        $_SESSION[configClass::$c_currentURI] = $uri;
    }

    public static function clearMessage()
    {
        unset($_SESSION[configClass::$c_msjTitle]);
        unset($_SESSION[configClass::$c_msjText]);
        unset($_SESSION[configClass::$c_msjType]);
    }

    public static function setMessage($xmodal, $xtitle, $xmessage, $xtype)
    {
        if ($xmodal) {
            $_SESSION[configClass::$c_msjTitle] = $xtitle;
            $_SESSION[configClass::$c_msjText] = $xmessage;
            $_SESSION[configClass::$c_msjType] = configClass::$arrayMsjType[$xtype];
        } else {
            $view_alert = View::factory('includes/message_alert');
            $view_alert->set(configClass::$c_msjTitle, $xtitle);
            $view_alert->set(configClass::$c_msjText, $xmessage);
            $view_alert->set(configClass::$c_msjType, configClass::$arrayMsjType[$xtype]);
            return $view_alert;
        }
    }


    public static function checkMessage()
    {
        return isset($_SESSION[configClass::$c_msjText]);
    }

    public static function addMonth($xyear, $xmonth, $xadd)
    {
        for ($i = 0; $i < $xadd; $i++) {
            $xmonth++;
            if ($xmonth > 12) {
                $xmonth = 1;
                $xyear++;
            }
        }
        return $resutl = array($xyear, $xmonth);
    }


    public static function checkExist($xvar, $xindex = null, $xdefault)
    {
        if ($xindex == null)
            return ($xvar != null) ? $xvar : $xdefault;
        else
            return (Arr::get($xvar, $xindex) != null) ? Arr::get($xvar, $xindex) : $xdefault;
    }

    public static function saveLog($xevent, $xtext, $xtype='I')
    {
        $objOrm = ORM::factory('log');
        $objOrm->Date = utilClass::getToday(true);
        $objOrm->Event = $xevent;
        $objOrm->Text = $xtext;
        $objOrm->Typr = $xtype;

        $objOrm->saveObj($objOrm);
    }

    public static function printLog($xtext, $xtype='I')
    {
       switch($xtype){
           case 'S':
               $class = 'text-success';break;
           case 'E':
               $class = 'text-danger';break;
           default:
               $class = '';break;
       }
       return '<span class="'.$class.'">[ ' . date('d-m-y H:i:m') . ' ] </span>  ' . $xtext . '<br>';
    }


    // ------------------------------------------------------------------------------------------------*
    // ---------------------------------- Methods Utils -----------------------------------------------*

    public static function uploadImage($xname, $xfolder)
    {
        $allowed = array('png', 'jpg', 'gif', 'zip');
        if (isset($_FILES[$xname]) && $_FILES[$xname]['error'] == 0) {

            $extension = pathinfo($_FILES[$xname]['name'], PATHINFO_EXTENSION);

            if (!in_array(strtolower($extension), $allowed))
                return null;

            $destination = $xfolder . md5($_FILES[$xname]['name']) . '.' . $extension;
            if (move_uploaded_file($_FILES[$xname]['tmp_name'], $destination))
                return $destination;
            else
                return null;
        }
    }
    public static function uploadCsv($xfile, $xfilename, $xfolder)
    {
        $allowed = array('csv');
        if (isset($_FILES[$xfile]) && $_FILES[$xfile]['error'] == 0) {

            $extension = pathinfo($_FILES[$xfile]['name'], PATHINFO_EXTENSION);

            if (!in_array(strtolower($extension), $allowed))
                return null;

            $destination = $xfolder . $xfilename . '.' . $extension;
            if (move_uploaded_file($_FILES[$xfile]['tmp_name'], $destination))
                return $destination;
            else
                return null;
        }
    }

    public static function sendEmail($xto, $xsubjet, $xbody)
    {
        $from = configClass::$emailFrom;

        $headers = "From: " . configClass::$emailFromName . '<' . $from . ">\r\n";
        $headers .= "Reply-To: " . $from . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        mail($xto, $xsubjet, $xbody, $headers);
    }

    public static function calculateAge($xdate)
    {
        list($Y, $m, $d) = explode("-", $xdate);
        return (date("md") < $m . $d ? date("Y") - $Y - 1 : date("Y") - $Y);
    }

    public static function isAutorizado($xtype)
    {
        $session = self::getUserSession();
        if ($session == null) {
            self::setMessage(true, '', 'No pose permisos para esta opcion', '');
            header('Location: ' . URL::base(true) . 'user/logon');
            die;
        }
        if ($session->getType() == configClass::$typeStandard && $session->getNeedActivate()) {
            header('Location: ' . URL::base(true) . 'user/activate');
            die;
        }

        if ($session->getType() >= $xtype)
            return true;
        else {
            self::setMessage(true, '', 'No pose permisos para esta opcion', '');
            header('Location: ' . URL::base(true) . 'user/logon');
            die;
        }
    }

    public static function getUserSession($xredirec = true)
    {
        if (Arr::get($_SESSION, configClass::$c_varSession) != null)
            return unserialize(Arr::get($_SESSION, configClass::$c_varSession));
        else {
            if ($xredirec) {
                header('Location: ' . URL::base(true));
                die;
            }
            return null;
        }
    }

    public static function setUserSession($xobj)
    {
        $_SESSION['is_user'] = true;
        $_SESSION[configClass::$c_varSession] = serialize($xobj);
    }

    public static function isAdmin()
    {
        $session = self::getUserSession();
        if ($session == null)
            return false;
        if ($session->getType() >= configClass::$typeAdmin)
            return true;
        return false;
    }

    public static function getPeriod($xtype)
    {
        $period = '';
        $timeIni = ' 00:00:00';
        $timeFin = ' 12:59:59';
        if ($xtype == 1) { //---------------------------- TODAY
            $today = self::getToday();
            $period[0] = $today . $timeIni;
            $period[1] = $today . $timeFin;
        } elseif ($xtype == 2) { //------------------------ MONTH
            $fecha = new DateTime();
            $fecha->modify('first day of this month');
            $fecha = $fecha->setTime(0, 0, 0);
            $period[0] = $fecha->format('Y-m-d H:i:s');
            $fecha = new DateTime();
            $fecha->modify('last day of this month');
            $fecha = $fecha->setTime(12, 59, 59);
            $period[1] = $fecha->format('Y-m-d H:i:s');
        } elseif ($xtype == 9) { //------------------------ INFINIT
            $period[0] = date("Y-m-d H:i:s", strtotime('1969-12-31'));
            $period[1] = date("Y-m-d H:i:s", strtotime('2020-12-31'));
        }
        return $period;
    }

    public static function debug($xorm = null)
    {
        if ($xorm == null) {
            echo 'Ultima consulta<br>' . Database::instance()->last_query;
        } else {
            echo 'Ultima consulta<br>' . $xorm->_db->last_query;
        }
        echo '<hr><br>Usuario logeado</br>';
        var_dump(utilClass::getUserSession(false));

        echo '<hr><br>Var Session</br>';
        var_dump($_SESSION);
        exit;
    }

    public static function clearXXS($xstr = null)
    {
        if ($xstr == null) {
            foreach ($_POST as $key => $value)
                $_POST[$key] = self::clearXXSAux($value);
        } else
            return self::clearXXSAux($xstr);
    }

    private static function clearXXSAux($xstr)
    {
        $xstr = strip_tags($xstr);
        $xstr = str_replace("'", "", $xstr);
        $xstr = str_replace('"', '', $xstr);
        $xstr = str_replace(';', '', $xstr);
        $xstr = preg_replace('/union/i', '', $xstr);
        $xstr = str_replace('=', '', $xstr);
        return $xstr;
    }

    public static function createPopoverConfirm($xurlAction, $xtextAction, $xdirection)
    {
        $html = "data-toggle='popover' data-html='true' data-placement='" . $xdirection . "' ";
        $html .= "data-content=\"<div class='w-m'>" . __('¿Realmente decea :a?', array(':a' => $xtextAction));
        $html .= "<div class='clearfix m-b-sm'></div>";
        $html .= "<a href='" . $xurlAction . "' class='btn btn-primary mr10'><i class='fa fa-check'></i></a>";
        $html .= "<a href='#' class='btn btn-default' onclick='closePopover()'><i class='fa fa-times'></i></a>";
        $html .= "</div>\" data-original-title='" . self::setUppercaseFirst($xtextAction) . "'";
        return $html;
    }

    public static function checkCpf($xcpf)
    {
        $cpf = str_pad(preg_replace('/\D/i', '', $xcpf), 11, '0', STR_PAD_LEFT);
        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999')
            return false;
        else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }

    public static function checkMobileBrazil($xmobile)
    {
        return preg_match('/\(([1-9]{1}\d{1})\)\s*([6-9]{1}\d{7,8})/i', $xmobile);
    }

    public static function getActiveLabel($xvalue)
    {
        $html = "<span class='label ";
        $html .= ($xvalue) ? "label-success" : "label-danger";
        $html .= "'>";
        $html .= ($xvalue) ? __('Activo') : __('Desactivo');
        $html .= "</span>";
        return $html;
    }

    public static function getNextPrevKey($xtype, $xdir, $xkey)
    {
        if ($xdir == null)
            return $xkey;

        $keyNew = $xkey;
        if ($xtype == 'quiz') {
            if ($xdir == 'up')
                $keyNew = $xkey + 1;
            elseif ($xdir == 'down')
                $keyNew = $xkey - 1;
            if ($keyNew <= 0 || ORM::factory('quiz')->getObj($keyNew) == null)
                $keyNew = $xkey;
        } else if ($xtype == 'month') {
            if ($xdir == 'up')
                $keyNew = date('Y-m', strtotime($xkey . '-01 +1 months'));
            elseif ($xdir == 'down')
                $keyNew = date('Y-m', strtotime($xkey . '-01 -1 months'));
            if ($keyNew <= date('Y-m', strtotime('01/11/2013')) || $keyNew > date('Y-m'))
                $keyNew = $xkey;
        }
        return $keyNew;
    }

    public static function checkDateTime($xdate, $xwithTime = false)
    {
        if ($xwithTime)
            return preg_match('/\d{2}-\d{2}-\d{4}\s\d{2}:\d{2}/i', $xdate);
        return preg_match('/\d{2}-\d{2}-\d{4}/i', $xdate);
    }

    public static function check18YearAgo($xdate)
    {
        $then = strtotime($xdate);
        $min = strtotime('+18 years', $then);
        return !(time() < $min);
    }

    public static function getListError($xexeption){
        $message = '<ul>';
        $errors = $xexeption->errors('model');
        foreach($errors as $field=>$value){
            $message .= '<li><b>' . __($field) . ':</b>  ' . $value .'</li>';
        }
        $message .= '</ul>';
        return $message;
    }

    public static function getTextHttpError($xcode)
    {
        if ($xcode !== NULL) {
            switch ($xcode) {
                case 100:
                    $text = 'Continue';
                    break;
                case 101:
                    $text = 'Switching Protocols';
                    break;
                case 200:
                    $text = 'OK';
                    break;
                case 201:
                    $text = 'Created';
                    break;
                case 202:
                    $text = 'Accepted';
                    break;
                case 203:
                    $text = 'Non-Authoritative Information';
                    break;
                case 204:
                    $text = 'No Content';
                    break;
                case 205:
                    $text = 'Reset Content';
                    break;
                case 206:
                    $text = 'Partial Content';
                    break;
                case 300:
                    $text = 'Multiple Choices';
                    break;
                case 301:
                    $text = 'Moved Permanently';
                    break;
                case 302:
                    $text = 'Moved Temporarily';
                    break;
                case 303:
                    $text = 'See Other';
                    break;
                case 304:
                    $text = 'Not Modified';
                    break;
                case 305:
                    $text = 'Use Proxy';
                    break;
                case 400:
                    $text = 'Bad Request';
                    break;
                case 401:
                    $text = 'Unauthorized';
                    break;
                case 402:
                    $text = 'Payment Required';
                    break;
                case 403:
                    $text = 'Forbidden';
                    break;
                case 404:
                    $text = 'Not Found';
                    break;
                case 405:
                    $text = 'Method Not Allowed';
                    break;
                case 406:
                    $text = 'Not Acceptable';
                    break;
                case 407:
                    $text = 'Proxy Authentication Required';
                    break;
                case 408:
                    $text = 'Request Time-out';
                    break;
                case 409:
                    $text = 'Conflict';
                    break;
                case 410:
                    $text = 'Gone';
                    break;
                case 411:
                    $text = 'Length Required';
                    break;
                case 412:
                    $text = 'Precondition Failed';
                    break;
                case 413:
                    $text = 'Request Entity Too Large';
                    break;
                case 414:
                    $text = 'Request-URI Too Large';
                    break;
                case 415:
                    $text = 'Unsupported Media Type';
                    break;
                case 500:
                    $text = 'Internal Server Error';
                    break;
                case 501:
                    $text = 'Not Implemented';
                    break;
                case 502:
                    $text = 'Bad Gateway';
                    break;
                case 503:
                    $text = 'Service Unavailable';
                    break;
                case 504:
                    $text = 'Gateway Time-out';
                    break;
                case 505:
                    $text = 'HTTP Version not supported';
                    break;
                default:
                    $text = 'Unknown http status code "' . htmlentities($xcode) . '"';
                    break;
            }
        }
        return $text;
    }

}

?>