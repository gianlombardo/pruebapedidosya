<?php defined('SYSPATH') or die('No direct script access.');

class Controller_i extends Controller
{

    public function before()
    {
        parent::before();
        $this->template = View::factory('templates/template_base');
    }

    public function action_index()
    {
        if (utilClass::getUserSession(false) === null) {
            $template = View::factory('templates/template_blank');
            $viewLogin = View::factory('includes/logon');

            utilClass::setMetas($template, __('Login'), '', '');
            $template->content = $viewLogin;
            $this->response->body($template);
        } else
            $this->redirect('action/pedidosya');
    }

    public function action_login()
    {
        utilClass::clearXXS();
        $username = Arr::get($_POST, 'txt-username');
        $password = Arr::get($_POST, 'txt-password');
        $result = json_decode($this->callAPI("tokens?clientId=$username&clientSecret=$password"));
        if (isset($result->code)){
            utilClass::setMessage(true, __('Usuario o contraseñas no validos'), '', 'E');
            $this->redirect('');
        }
        else {
            $_SESSION[configClass::$c_varSession] = serialize($result->access_token);
            $this->redirect('action/pedidosya');
        }
    }

    public function action_logout()
    {
        session_destroy();
        session_unset();
        $this->redirect('');
    }

    public function action_action()
    {
        utilClass::getUserSession(true);
        $action = utilClass::clearXXS($this->request->param('id'));
        if (empty($action))
            throw HTTP_Exception::factory(404, 'The requested URL :uri was not found on this server.', array(':uri' => $this->request->uri()))->request($this->request);
        $view = View::factory('includes/' . $action);
        $this->template->content = $view;

        utilClass::setMetas($this->template, __($action), '', '');
        $this->response->body($this->template);
    }

    public function action_searchrestaurant()
    {
        utilClass::getUserSession(true);
        utilClass::clearXXS();
        $location = Arr::get($_POST, 'l');
        $fields = "name,logo,link,address,doorNumber";
        $max = 10;
        $offset = Arr::get($_POST, 'o');
        echo $this->callAPI("search/restaurants?max=$max&point=$location&fields=$fields&offset=$offset");
    }

    function callApi($xuri)
    {
        $urlBase = 'http://stg-api.pedidosya.com/public/v1/';
        $curl = curl_init();

        if (utilClass::getUserSession(false) !== null) {
            $headers = array('Authorization: ' . utilClass::getUserSession(false));
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        }
        curl_setopt($curl, CURLOPT_URL, $urlBase . $xuri);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function after()
    {
        parent::after();
    }


}
