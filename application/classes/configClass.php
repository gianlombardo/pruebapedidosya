<?php defined('SYSPATH') OR die('No Direct Script Access');

class configClass
{
    public static $delimiterFile = ';';

    // Paths
    public static $path_css = "files/css/";
    public static $path_js = "files/js/";
    public static $path_img = "files/img/";
    public static $path_upload = "./files/uploads/";
    public static $path_downloads = "./files/downloads/";

    // Default
    public static $titleDefault = "Inicio";
    public static $desDefault = "";
    public static $keywordDefault = "";
    public static $avatarDefault = "avatar.png";

    // Static Texts
    public static $c_msjTitle = "msjTitle";
    public static $c_msjText = "msjText";
    public static $c_msjType = "msjType";
    public static $c_lastQuery = "lastQuery";
    public static $c_currentURI = "currentURI";
    public static $c_sepLine = "#:#";
    public static $c_sepField = "@:@";
    public static $c_nameApp = "PruebaPedidosYa";
    public static $c_varSession = "pediddosya_session";

    // Paginated
    public static $perPage = 20;
    public static $cantItems = 5;

    // Status
    public static $active = 1;
    public static $disable = 0;
    public static $processed = 1;
    public static $unprocessed = 0;

    // Types Users
    public static $typeStandard = 1;
    public static $typeAdmin = 9;

    public static $arrayMonth = array('', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    public static $arraySex = array('H' => 'Masculino', 'M' => 'Femenino', '' => '');
    public static $arrayMsjType = array('I' => 'info', 'W' => 'warning', 'E' => 'error', 'S' => 'success', '' => '');

}

?>