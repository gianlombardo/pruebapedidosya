<section class="panel bg-white no-b">
    <div class="p15">
        <img src="http://www.pedidosya.com.uy/assets/common/logo-es-ccbd956403c32ca5f4a2189b3194e4d1.svg" class="text-center center-block mb10" width="151px">

        <? if (isset($_SESSION[configClass::$c_msjText])) { ?>
            <div class="animated shake mb15">
                    <h4 class="text-danger ml5 mt0 text-center">
                    <i class="fa fa-exclamation-circle text-danger"></i>
                        <?= __($_SESSION[configClass::$c_msjTitle]) ?></h4>
            </div>
            <? utilClass::clearMessage();
        } ?>

        <form role="form" action="<?=URL::base(true)?>login" method="post">
            <input type="text" name="txt-username" class="form-control input-lg mb25" placeholder="Username" autofocus value="pruebaphp">
            <input type="password"  name="txt-password" class="form-control input-lg mb25" placeholder="Password">

            <button class="btn btn-primary btn-lg btn-block" type="submit"><?=__('Entrar')?></button>
        </form>
    </div>
</section>