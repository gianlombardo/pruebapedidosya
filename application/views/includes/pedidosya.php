<div id="map-pedidosya">

</div>
<div id="map-results">
    <ul>
    </ul>
</div>

<div id="template-li">
    <li class="row animated fadeInUp">
        <div class="col-xs-3">
            <img src="">
        </div>
        <div class="col-xs-8">
            <h4></h4>
            <span class="text-muted"></span>
        </div>
    </li>
</div>

<script type="application/javascript">

    $(document).ready(function () {
        var map;
        var marker;
        var offset;
        var point;

        function initialize() {
            var mapOptions = {
                zoom: 11,
                center: new google.maps.LatLng(-34.8149838, -56.1870976)
            };
            map = new google.maps.Map(document.getElementById('map-pedidosya'), mapOptions);
            google.maps.event.addListener(map, 'click', function (event) {
                $('#map-results ul').html('');
                offset=0;
                placeMarker(event.latLng);
            });
        }

        function placeMarker(location) {
            if (marker) {
                marker.setPosition(location);
            } else {
                marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
            }
            point = location.k + ',' + location.D;
            searchRestaurant();
        }

        function searchRestaurant() {
            NProgress.start();
            $.ajax({
                url: getBaseURL() + 'searchrestaurant',
                type: "POST",
                data: {l: point, o:offset},
                success: function (html) {
                    offset+=10;
                    listResult(html)
                    NProgress.done();
                },
                error: function (jqXHR) {
                    NProgress.done();
                    console.log("ajax error " + jqXHR.status);
                }
            });
        }

        function listResult(xhtml) {
            var json = JSON.parse(xhtml);
            var ul = $('#map-results ul');
            ul.find('.btn-seemore').closest('li').remove();
            if (json.total > 0) {
                json.data.forEach(function (entity) {
                    var li = $('#template-li').clone();
                    li.find('img').attr('src', 'https://d3h2p015ljvhse.cloudfront.net/images/restaurants/' + entity.logo);
                    li.find('h4').html(entity.name);
                    li.find('span').html(entity.address + " " + entity.doorNumber);
                    ul.append(li);
                });

                ul.append("<li class='p25'><a href='#' class='btn-seemore btn btn-primary btn-block'>Ver mas...</a></li>");
                $('.btn-seemore').click(function(){
                    searchRestaurant();
                });
            }
        }



        google.maps.event.addDomListener(window, 'load', initialize);
    });
</script>