<!doctype html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="description" content="<?= $desPage ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

    <title><?= $titlePage ?> - PruebaPedidosYa</title>


    <link rel="stylesheet" href="<?= utilClass::getUrlResource(1) ?>main.css">

    <!--[if lt IE 9]>
    <script src="<?=utilClass::getUrlResource(2)?>html5shiv.js"></script>
    <script src="<?=utilClass::getUrlResource(2)?>respond.min.js"></script>
    <![endif]-->

    <? utilClass::setCurrentURI(); ?>
    <script src="<?= utilClass::getUrlResource(2) ?>jquery.min.js"></script>
    <script src="<?= utilClass::getUrlResource(2) ?>bootstrap.js"></script>
    <script src="<?= utilClass::getUrlResource(2) ?>nprogress.js"></script>
</head>

<body>
<div class="app">

    <header class="header header-fixed navbar">
        <div class="brand">
            <a href="javascript:;" class="ti-menu off-left visible-xs" data-toggle="offscreen" data-move="ltr"></a>
            <a href="<?= URL::base(true) ?>" class="navbar-brand">
                <img src="http://www.pedidosya.com.uy/assets/common/logo-es-ccbd956403c32ca5f4a2189b3194e4d1.svg" alt="PedidosYa" >
            </a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li class="off-right">
                <a href="#">
                    <img src="<?= utilClass::getUrlResource(3) ?>avatar.jpg" class="header-avatar img-circle" alt="user"
                         title="user">
                    <span class="hidden-xs ml10">
                        pruebaphp
                    </span>
                </a>
            </li>
            <li>
                <a href="<?=URL::base(true)?>logout">
                    <i class="fa fa-sign-out"></i>
                </a>
            </li>
        </ul>
    </header>

    <section class="layout">
        <aside class="sidebar">
            <nav class="main-navigation" data-height="auto" data-size="6px" data-distance="0" data-rail-visible="true"
                 data-wheel-step="10">
                <p class="nav-title">MENU</p>
                <ul class="nav">

                    <li>
                        <a href="<?= URL::base(true) ?>action/home">
                            <i class="fa fa-dot-circle-o"></i>
                            <span><?= __('home') ?></span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= URL::base(true) ?>action/aboutme">
                            <i class="fa fa-dot-circle-o"></i>
                            <span><?= __('aboutme') ?></span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= URL::base(true) ?>action/php">
                            <i class="fa fa-dot-circle-o"></i>
                            <span><?= __('php') ?></span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= URL::base(true) ?>action/pedidosya">
                            <i class="fa fa-dot-circle-o"></i>
                            <span><?= __('pedidosya') ?></span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= URL::base(true) ?>action/mysql">
                            <i class="fa fa-dot-circle-o"></i>
                            <span><?= __('mysql') ?></span>
                        </a>
                    </li>
                </ul>
            </nav>
        </aside>


        <section class="main-content">
            <div class="content-wrap">


                    <!---------------------- CONTENT -------------------------->
                    <?= $content ?>
                    <!------------------- END CONTENT ------------------------>


            </div>
            <a class="exit-offscreen"></a>
        </section>
    </section>
</div>

<script src="<?= utilClass::getUrlResource(2) ?>bootstrap.js"></script>
<script src="<?= utilClass::getUrlResource(2) ?>jquery.slimscroll.min.js"></script>
<script src="<?= utilClass::getUrlResource(2) ?>jquery.easing.min.js"></script>
<script src="<?= utilClass::getUrlResource(2) ?>jquery.appear.js"></script>
<script src="<?= utilClass::getUrlResource(2) ?>jquery.placeholder.js"></script>
<script src="<?= utilClass::getUrlResource(2) ?>fastclick.js"></script>
<script src="<?= utilClass::getUrlResource(2) ?>offscreen.app.js"></script>
<script src="<?= utilClass::getUrlResource(2) ?>toastr.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

<script src="<?= utilClass::getUrlResource(2) ?>main.app.js"></script>
<script src="<?= utilClass::getUrlResource(2) ?>custom.app.js"></script>

<script type="application/javascript">
    $(document).ready(function () {
        <? if (isset($_SESSION[configClass::$c_msjTitle])){?>
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "swing",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        toastr['<?=$_SESSION[configClass::$c_msjType]?>']('<?=$_SESSION[configClass::$c_msjText]?>', '<?=$_SESSION[configClass::$c_msjTitle]?>');
        <? utilClass::clearMessage()?>
        <? } ?>
    });
</script>


</body>

</html>
