<!doctype html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="description" content="<?=$desPage?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

    <title><?=$titlePage?> - Prueba PedidosYa</title>


    <link rel="stylesheet" href="<?=utilClass::getUrlResource(1)?>main.css">


    <!--[if lt IE 9]>
    <script src="<?=utilClass::getUrlResource(2)?>html5shiv.js"></script>
    <script src="<?=utilClass::getUrlResource(2)?>respond.min.js"></script>
    <![endif]-->

    <? utilClass::setCurrentURI(); ?>
</head>
<body class="bg-primary">
<div class="center-wrapper">
    <div class="center-content">
        <div class="row">
            <? $col = (isset($col)) ? $col : 4;
            $offset = (12 - $col) / 2;
            ?>
            <div class="col-md-<?= $col ?> col-md-offset-<?= $offset ?>">

                <!---------------------- CONTENT -------------------------->
                <?= $content?>
                <!------------------- END CONTENT ------------------------>
            </div>
        </div>
    </div>
</div>
</body>
</html>
