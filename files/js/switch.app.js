/**
 * @author Gianfranco Lombardo
 * -------------------------------------
 *
 */

(function ($) {
    $.fn.extend({
        switch: function () {
            return this.each(function () {
                var elem = document.querySelector('.switchery');
                var switchery = new Switchery($(this).get(), {color: '#17c3e5'});
                $('.switchery').click(function () {
                    var value = $(this).prev('input').val();
                    value = (value == 'on' ? false : value);
                    value = (value == 'true' ? false : true);
                    $(this).prev('input').val(value);
                });
            });
        }
    });
})(jQuery)