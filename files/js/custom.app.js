/*Functions --------------------------------------------------------------------------------------------------------*/
function getController() {
    var url = window.location.href.replace('#', '');
    url = url.split('//').join('/')
    var arr = url.split("/");
    var result = Array();
    result[0] = ( arr[3] != null ? arr[3] : '' );
    result[1] = ( arr[4] != null ? arr[4] : '' );
    //console.log(result);
    return result;
}

function getBaseURL() {
    var url = location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14));
    if (baseURL.indexOf('http://localhost') != -1 || baseURL.indexOf('http://127.0.0.1') != -1) {
        var url = location.href;
        var pathname = location.pathname;
        var index1 = url.indexOf(pathname);
        var index2 = url.indexOf("/", index1 + 1);
        var baseLocalUrl = url.substr(0, index2);
        return baseLocalUrl + "/";
    }else{
        var subfolder = (location.href.search('syna')) > 0 ? '/syna' : '';
        return baseURL + subfolder + "/";
    }
}

function setToastrConfig() {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "500",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
}



/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------- READY -----------------------------------------------------*/
$(document).ready(function () {
    var arr = getController();
    var controller = arr[0];
    var action = arr[1];

    /* Init progress bar */
    $('.progress-bar').each(function(){
        $(this).width($(this).attr('data-value') + '%');
    });

    /*---------------------------------------------------------------------------------------------*/
    /*---------------------------------------------------------------------------------------------*/
    /*--------------------------------------- upload_callback -------------------------------------*/
    if (controller == 'action' && action == 'home') {

    }

});